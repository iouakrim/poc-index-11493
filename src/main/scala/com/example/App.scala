package com.example

import cats.effect.IO
import cats.effect.IOApp
import org.typelevel.log4cats.SelfAwareStructuredLogger
import org.typelevel.log4cats.slf4j.Slf4jLogger

object App extends IOApp.Simple {
  val logger: SelfAwareStructuredLogger[IO] = Slf4jLogger.getLogger[IO]

  override def run: IO[Unit] =
    for {
      _ <- IO.delay(println(s"App running"))
      processor = new Processor[IO]()
      // _     <- processor.putObject("org2/receipt.pdf", new ByteArrayInputStream(bytes), "application/pdf")
      t0 = System.currentTimeMillis()
      // _ <- processor.exportPdfFromCsvWithJReport()
      // t1 = System.currentTimeMillis()
      // _  = println("Csv with jReport take: " + (t1 - t0) + "ms")
      // _ <- processor.exportPdfFromCsvWithItext()
      // t2 = System.currentTimeMillis()
      // _  = println("Csv with Itext take: " + (t2 - t1) + "ms")
      // _ <- processor.exportPdfFromJsonWithJReport()
      // t3 = System.currentTimeMillis()
      // _  = println("Json with JReport take: " + (t3 - t2) + "ms")
      // _ <- processor.exportPdfFromHtmlWithJReport()
      // t4 = System.currentTimeMillis()
      // _  = println("Html with JReport take: " + (t4 - t3) + "ms")
      // _ <- processor.exportPdfFromHtmlWithItext()
      // t5 = System.currentTimeMillis()
      // _  = println("Html with iText take: " + (t5 - t4) + "ms")
      _ <- processor.exportPdfFromHtmlWithItext2()
      t6 = System.currentTimeMillis()
      _  = println("Html with iText take: " + (t6 - t0) + "ms")
    } yield ()

}
