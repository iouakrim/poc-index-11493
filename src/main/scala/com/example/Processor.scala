package com.example

import cats.effect.Async
import com.itextpdf.html2pdf.ConverterProperties
import com.itextpdf.html2pdf.HtmlConverter
import com.itextpdf.kernel.geom.PageSize
import com.itextpdf.kernel.pdf.PdfDocument
import com.itextpdf.kernel.pdf.PdfWriter
import net.sf.jasperreports.engine.JREmptyDataSource
import net.sf.jasperreports.engine.JasperCompileManager
import net.sf.jasperreports.engine.JasperExportManager
import net.sf.jasperreports.engine.JasperFillManager
import net.sf.jasperreports.engine.data.JRCsvDataSource
import net.sf.jasperreports.engine.data.JsonDataSource

import java.io.File
import java.io.FileOutputStream
import java.io.InputStream
import java.net.URL
import java.util
import scala.io.Source

class Processor[F[_]: Async]() {
  val html =
    "<html><head><meta http-equiv='Content - Type' content='text / html; charset = utf - 8'><style>body{margin-top:0px;margin-left:0px}#m_page_1{overflow:hidden;margin:186px 0px 184px 190px;padding:0px;border:none;width:603px}#m_page_1 #m_p1dimg1{width:300px;height:151px}#m_page_1 #m_p1dimg1 #m_p1img1{width:300px;height:151px}.m_ft0{font:bold 12px 'Arial';line-height:15px}.m_ft1{font:10px 'Arial';line-height:13px}.m_ft2{font:12px 'Arial';line-height:15px}.m_ft3{font:9px 'Arial';line-height:11px}.m_ft4{font:bold 12px 'Arial';line-height:13px}.m_ft5{font:10px 'Arial';line-height:11px}.m_ft6{font:1px 'Arial';line-height:11px}.m_ft7{font:9px 'Arial';line-height:12px}.m_ft8{font:10px 'Arial';line-height:12px}.m_ft9{font:bold 12px 'Arial';line-height:14px}.m_ft10{font:1px 'Arial';line-height:1px}.m_ft11{font:8px 'Arial';line-height:10px}.m_p0{text-align:left;padding-left:162px;margin-top:0px;margin-bottom:0px}.m_p1{text-align:left;padding-left:175px;margin-top:15px;margin-bottom:0px}.m_p2{text-align:left;padding-left:164px;margin-top:1px;margin-bottom:0px}.m_p3{text-align:left;padding-left:168px;margin-top:0px;margin-bottom:0px}.m_p4{text-align:left;padding-left:138px;margin-top:0px;margin-bottom:0px}.m_p5{text-align:left;padding-left:178px;margin-top:14px;margin-bottom:0px}.m_p6{text-align:left;padding-right:185px;margin-top:10px;margin-bottom:0px}.m_p7{text-align:left;padding-right:499px;margin-top:0px;margin-bottom:0px}.m_p8{text-align:left;margin-top:0px;margin-bottom:0px}.m_p9{text-align:left;margin-top:0px;margin-bottom:0px;white-space:nowrap}.m_p10{text-align:right;margin-top:0px;margin-bottom:0px;white-space:nowrap}.m_p11{text-align:right;padding-right:1px;margin-top:0px;margin-bottom:0px;white-space:nowrap}.m_p12{text-align:left;margin-top:9px;margin-bottom:0px}.m_p13{text-align:left;margin-top:149px;margin-bottom:0px}.m_p14{text-align:left;margin-top:1px;margin-bottom:0px}.m_p15{text-align:left;padding-left:176px;margin-top:0px;margin-bottom:0px;white-space:nowrap}.m_td0{padding:0px;margin:0px;width:354px;vertical-align:bottom}.m_td1{padding:0px;margin:0px;width:60px;vertical-align:bottom}.m_td2{padding:0px;margin:0px;width:244px;vertical-align:bottom}.m_td3{padding:0px;margin:0px;width:170px;vertical-align:bottom}.m_td4{padding:0px;margin:0px;width:166px;vertical-align:bottom}.m_td5{padding:0px;margin:0px;width:248px;vertical-align:bottom}.m_tr0{height:14px}.m_tr1{height:11px}.m_tr2{height:13px}.m_tr3{height:23px}.m_tr4{height:12px}.m_tr5{height:25px}.m_tr6{height:15px}.m_tr7{height:16px}.m_tr8{height:24px}.m_t0{width:414px;margin-top:10px;font:10px 'Arial'}.m_t1{width:414px;font:10px 'Arial'}.m_t2{width:414px;margin-top:9px;font:10px 'Arial'}</style></head><body><u></u><div><div id='m_page_1'><p class='m_p0 m_ft0'>CARD HOLDER</p><p class='m_p1 m_ft1'></p><p class='m_p2 m_ft2'>MBL France Mer</p><p class='m_p3 m_ft1'>paris</p><p class='m_p4 m_ft1'>Haguenau 110061</p><p class='m_p6 m_ft3'>TERM# <u></u>ingenico-DX8000-215MCD824319MID#<u></u> <u></u>f4be5a12-bb4a-46d4-b6ca-1ad8d6e4b5be<u></u> mblfrance.com<u></u>V1.13.0-SNAPSHOT<u></u></p><p class='m_p7 m_ft4'>5226********6290 <span class='m_ft1'>attendantId</span></p><p class='m_p12 m_ft0'>SALE</p><table cellpadding = '0' cellspacing='0' class='m_t1'><tbody><tr><td class='m_tr2 m_td2'><p class='m_p9 m_ft1'>BATCH</p></td><td class='m_tr2 m_td3'><p class='m_p11 m_ft1'>1234</p></td></tr><tr><td class='m_tr2 m_td2'><p class='m_p9 m_ft1'>TRACE NO</p></td><td class='m_tr2 m_td3'><p class='m_p11 m_ft1'>5678</p></td></tr><tr><td class='m_tr2 m_td2'><p class='m_p9 m_ft1'>REF NO</p></td><td class='m_tr2 m_td3'><p class='m_p11 m_ft1'>9234</p></td></tr></tbody></table><p class='m_p12 m_ft0'>REWARD</p><table cellpadding = '0' cellspacing='0' class='m_t1'><tbody><tr><td class='m_tr2 m_td2'><p class='m_p9 m_ft1'>REF NO</p></td><td class='m_tr2 m_td3'><p class='m_p11 m_ft1'>0</p></td></tr><tr><td class='m_tr2 m_td2'><p class='m_p9 m_ft1'>ORIGINAL AMOUNT</p></td><td class='m_tr2 m_td3'><p class='m_p11 m_ft1'>0</p></td></tr></tbody></table><br/><table cellpadding = '0' cellspacing='0' class='m_t1'><tbody><tr><td class='m_tr2 m_td2'><p class='m_ft0 m_p9'>BASE</p></td><td class='m_tr2 m_td3'><p class='m_ft0 m_p11'>100</p></td></tr></tbody></table><table cellpadding = '0' cellspacing='0' class='m_t1'><tbody></tbody></table><table cellpadding = '0' cellspacing='0' class='m_t1'><tbody><tr><td class='m_tr2 m_td2'><p class='m_ft0 m_p9'>TIP</p></td><td class='m_tr2 m_td3'><p class='m_ft0 m_p11'>0</p></td></tr></tbody></table><table cellpadding = '0' cellspacing='0' class='m_t1'><tbody></tbody></table><table cellpadding = '0' cellspacing='0' class='m_t1'><tbody><tr><td class='m_tr2 m_td2'><p class='m_ft0 m_p9'>PNBP</p></td><td class='m_tr2 m_td3'><p class='m_ft0 m_p11'>0</p></td></tr><tr><td class='m_tr6 m_td2'><p class='m_p9 m_ft10'>&nbsp;</p></td><td class='m_tr6 m_td3'><p class='m_p11 m_ft0'><u></u>---------------<u></u></p></td></tr></tbody></table><table cellpadding = '0' cellspacing='0' class='m_t1'><tbody></tbody></table><table cellpadding = '0' cellspacing='0' class='m_t1'><tbody><tr><td class='m_tr2 m_td2'><p class='m_ft0 m_p9'>TOTAL</p></td><td class='m_tr2 m_td3'><p class='m_ft0 m_p11'>100</p></td></tr></tbody></table><table cellpadding = '0' cellspacing='0' class='m_t1'><tbody></tbody></table><p class='m_p12 m_ft0'>INSTALLMENT PROMO</p><table cellpadding = '0' cellspacing='0' class='m_t1'><tbody><tr><td class='m_tr2 m_td2'><p class='m_p9 m_ft1'>PLAN</p></td><td class='m_tr2 m_td3'><p class='m_p11 m_ft1'>001</p></td></tr><tr><td class='m_tr2 m_td2'><p class='m_p9 m_ft1'>TENOR</p></td><td class='m_tr2 m_td3'><p class='m_p11 m_ft1'>06</p></td></tr><tr><td class='m_tr2 m_td2'><p class='m_p9 m_ft1'>MONTHLY PMT</p></td><td class='m_tr2 m_td3'><p class='m_p11 m_ft1'>EUR</p></td></tr><tr><td class='m_tr2 m_td2'><p class='m_p9 m_ft1'>DESCRIPTION</p></td><td class='m_tr2 m_td3'><p class='m_p11 m_ft1'>some text here</p></td></tr></tbody></table><p class='m_p14 m_ft1' style='text-align:center;'></p><p class='m_p13 m_ft8'>SIGN X_____________________________<wbr>____</p><p class='m_p14 m_ft11'>I AGREE TO PAY ABOVE TOTAL AMOUNT ACCORDING TO CARD ISSUER AGREEMENT</p><table cellpadding = '0' cellspacing= '0' class='m_t2'><tbody><tr><td class='m_tr0 m_td4'><p class='m_p9 m_ft1'>TRN</p></td><td class='m_tr0 m_td5'><p class='m_p9 m_ft10'>&nbsp;</p></td></tr><tr><td class='m_tr4 m_td4'><p class='m_p9 m_ft8'>APPNAME</p></td><td class='m_tr4 m_td5'><p class='m_p15 m_ft7'>MEMORY INFO</p></td></tr><tr><td class='m_tr2 m_td4'><p class='m_p9 m_ft10'>&nbsp;</p></td><td class='m_tr2 m_td5'><p class='m_p9 m_ft1'></p></td></tr></tbody></table></div></div></body></html>"

  // CSV to PDF using JasperReport
  def exportPdfFromCsvWithJReport(): F[Unit] =
    Async[F].blocking {
      val jrFile: InputStream  = getClass.getResourceAsStream("/cars2.jrxml")
      val csvFile: InputStream = getClass.getResourceAsStream("/cars.csv")
      val jasperReport         = JasperCompileManager.compileReport(jrFile)
      val t11                  = System.currentTimeMillis()
      val parameters           = new util.HashMap[String, Object]()
      val columns              = Array("id", "name", "price")
      val csvDataSource        = new JRCsvDataSource(csvFile, "UTF-8")
      csvDataSource.setUseFirstRowAsHeader(true)
      csvDataSource.setRecordDelimiter("\n")
      csvDataSource.setColumnNames(columns)
      val jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, csvDataSource)
      JasperExportManager.exportReportToPdfFile(jasperPrint, "cars-JReport.pdf")
      val t12 = System.currentTimeMillis()
      println("Csv with jReport take 2: " + (t12 - t11) + "ms")
      // JasperExportManager.exportReportToPdf(jasperPrint) // return a bytes
    }

  // CSV TO PDF using itext
  def exportPdfFromCsvWithItext(): F[Unit] = {
    import com.itextpdf.text.Document
    import com.itextpdf.text.PageSize
    import com.itextpdf.text.Paragraph
    import com.itextpdf.text.Phrase
    import com.itextpdf.text.pdf.PdfPCell
    import com.itextpdf.text.pdf.PdfPTable
    import com.itextpdf.text.pdf.PdfWriter

    Async[F].blocking {
      val bufferedSource = Source.fromResource("cars.csv")
      val lines          = bufferedSource.getLines.toList

      val document  = new Document(PageSize.A4.rotate(), 25, 25, 25, 25)
      val pdfWriter = PdfWriter.getInstance(document, new FileOutputStream("cars-itext.pdf"))
      document.open()
      val header = new Paragraph("List of cars")
      document.add(header)

      val t = new PdfPTable(3)
      t.setSpacingBefore(25)
      t.setSpacingAfter(25)

      val isHeader = true
      for (record <- lines) {
        val line  = record.split(",").map(_.trim)
        val id    = line(0)
        val name  = line(1)
        val price = line(2)
        if (isHeader) {
          val c1 = new PdfPCell(new Phrase(id))
          t.addCell(c1)
          val c2 = new PdfPCell(new Phrase(name))
          t.addCell(c2)
          val c3 = new PdfPCell(new Phrase(price))
          t.addCell(c3)
        } else {
          val c1 = new PdfPCell(new Phrase(id))
          t.addCell(c1)
          val c2 = new PdfPCell(new Phrase(name))
          t.addCell(c2)
          val c3 = new PdfPCell(new Phrase(price))
          t.addCell(c3)
        }
      }
      document.add(t)
      document.close()
    }
  }

  // JSON to PDF using JasperReport
  def exportPdfFromJsonWithJReport(): F[Unit] =
    Async[F].blocking {
      val jrFile: InputStream = getClass.getResourceAsStream("/users.jrxml")
      val jasperReport        = JasperCompileManager.compileReport(jrFile)
      val parameters          = new util.HashMap[String, Object]()

      val url = "https://jsonplaceholder.typicode.com/users"
      val is  = new URL(url).openStream()
      val ds  = new JsonDataSource(is)

      val jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, ds)
      JasperExportManager.exportReportToPdfFile(jasperPrint, "users.pdf")
    }

  // HTML to PDF using jasper report
  def exportPdfFromHtmlWithJReport(): F[Unit] =
    Async[F].blocking {
      val jrFile: InputStream = getClass.getResourceAsStream("/receipt.jrxml")
      val jasperReport        = JasperCompileManager.compileReport(jrFile)
      val parameters          = new util.HashMap[String, Object]()

      parameters.put(
        "html",
        html
      )
      val jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, new JREmptyDataSource())
      JasperExportManager.exportReportToPdfFile(jasperPrint, "receipt-JReport.pdf")
      // JasperExportManager.exportReportToPdf(jasperPrint)
    }

  // HTML to PDF using itext ( pdfHtml )
  def exportPdfFromHtmlWithItext(): F[Unit] =
    Async[F].blocking {
      val pdfDest = new File("receipt-Itext.pdf")
      val output  = new FileOutputStream(pdfDest)
      val writer  = new PdfWriter(output)
      // val pdfDocument = new PdfDocument(writer)
      // val pageSize    = new PageSize(215, 842)
      // pdfDocument.setDefaultPageSize(PageSize.A4)
      HtmlConverter.convertToPdf(html, writer, new ConverterProperties())
    }
  def exportPdfFromHtmlWithItext2(): F[Unit] =
    Async[F].blocking {
      import com.itextpdf.html2pdf.ConverterProperties
      import com.itextpdf.html2pdf.HtmlConverter
      import com.itextpdf.layout.Document
      import java.io.FileOutputStream

      val html2 =
        """<!DOCTYPE html>
          |<html>
          |    <head>
          |        <title>Example</title>
          |    </head>
          |		<body><div class="receipt-body"><div class="receipt"><div><meta http-equiv="Content - Type" content="text / html; charset = utf - 8"><style>body {  font-weight: bold;  font-size: 12pt;  font-family: sans-serif;}.logo img {  display: block;  margin: auto;  max-width: 100%;}.block {  padding-bottom: 0px;}hr {  border-top: 2px dashed;}hr.space {  margin: 10px auto;}hr.underline {  margin: 0px auto;}div.space {  padding: 10px;}div.medium-space {  padding: 15px;}div.margin {  margin-left: 35px;  margin-right: 35px}div.super-space {  padding: 20px;}div.footer-space {  padding: 50px;}.line {  overflow-wrap: break-word;}.line, tr {  line-height: 1.4;}.title-line {  text-align: center;}.row {  display: flex;  line-height: 1.5;}.column:last-child {  margin-left: auto;  word-break: break-word;  text-align: right;}.block-list .block {  padding-bottom: 50px;}.block-list .block:first-child {  padding-top: 10px;}.block-list .block:last-child {  padding-top: unset;}.block .block:last-child {  padding-bottom: unset;}.highlight {  font-weight: 900;}.md {  font-size: 16pt;}.md14 {  font-size: 14pt;}.lg {  font-size: 16,5pt;}.uppercase {  text-transform: uppercase;}.normal-case {  text-transform: none;}.batch-header {  text-transform: uppercase;}.block:last-child > .batch-header {  text-align: center;}.block table.batch {  border-collapse: collapse;  width: 100%;}.block table.batch tr:first-child {  border-bottom: 0px solid;}.block table.batch td {  text-align: center;}.block table.batch td:first-child {  text-align: left;}.block table.batch td:last-child {  width: 55%;  text-align: right;}.block table.batch th {  font-weight: 500;  text-align: center;}.block table.batch th:first-child {  text-align: left;}.block table.batch th:last-child {  text-align: right;}#sign {  width: 80%;  margin: auto;}#sign > .column:last-child {  overflow: hidden;  white-space: normal;  word-break: unset;}div.margin-left {margin-left: auto;}div.margin-right {margin-right: auto;}div.center {margin: 0 auto;}.receipt{ font-size:12px;}.receipt span{ font-size:12px;}@media screen and (max-width: 767px) {.receipt{ font-size:12pt;}.receipt span{ font-size:12pt;}}</style><div id="logo" class="block logo"></div><div class="space"></div><div id="header" class="block header"><div class="block"><div class="block"><div class="line title-line md14"> </div></div></div><div class="block"><div class="block"><div class="line title-line md14"> </div></div></div><div class="block"><div class="block"><div class="line title-line md14"> </div></div></div><div class="block"><div class="block"><div class="line title-line md14"> </div></div></div><div class="block"><div class="block"><div class="line title-line md14"> </div></div></div><div class="block"><div class="block"><div class="line title-line highlight md"> CHIP</div></div></div><div class="block"><div class="block"><div class="block table.batch th:first-child md14"> A0000000031010</div></div></div><div class="block"><div class="block"><div class="block table.batch th:first-child md14"> </div></div></div><div class="block"><div class="block"><div class="block table.batch th:first-child md14"> 10/05/2022 11:45:43</div></div></div><div class="block"><div class="block"><div class="block table.batch th:first-child md14"> </div></div></div><div class="block"><div class="block"><div class="block table.batch th:first-child md14"> 121781</div></div></div><div class="block"><div class="block"><div class="block table.batch th:first-child md14"> 001</div></div></div><div class="block"><div class="block"><div class="block table.batch th:first-child md14"> 4761 73** **** 0029</div></div></div><div class="block"><div class="block"><div class="block table.batch th:first-child md14"> 7D12C55D9F0DD15A</div></div></div><div class="block"><div class="block"><div class="block table.batch th:first-child md14"> EZP-40b9e4dd-2730-423d-9717-3d0c98cd7c71</div></div></div><div class="block"><div class="block"><div class="block table.batch th:first-child md14"> </div></div></div><div class="row"><div class="column md14">AMOUNT</div><div class="column md14">EUR 2.58</div></div><div class="row"><div class="column highlight md">TOTAL AMOUNT</div><div class="column highlight md">EUR 2.58</div></div><div class="row"><div class="column highlight md">SALE</div><div class="column highlight md">APPROVED</div></div><div class="medium-space"></div><span></span></div></div><div class="footer-space"></div></div></div></div></body>
          |</html>""".stripMargin
      // Setting destination
      val fileOutputStream: FileOutputStream = new FileOutputStream(new File("receipt-Itext2.pdf"))

      val pdfWriter: PdfWriter                     = new PdfWriter(fileOutputStream)
      val converterProperties: ConverterProperties = new ConverterProperties
      val pdfDocument: PdfDocument                 = new PdfDocument(pdfWriter)

      // For setting the PAGE SIZE
      val pageSize = new PageSize(215, 400)
      pdfDocument.setDefaultPageSize(pageSize)
      // val document = new Document(pdfDocument)
      // document.setMargins(5, 5, 5, 5)
      val res: Document = HtmlConverter.convertToDocument(html2, pdfDocument, converterProperties)
      res.close()
    }

}
