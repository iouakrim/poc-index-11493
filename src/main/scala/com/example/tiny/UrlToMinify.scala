package com.example.tiny

import cats.Show
import cats.implicits.toShow
import io.circe.Codec
import io.circe.generic.semiauto.deriveCodec
import io.circe.syntax.EncoderOps

final case class UrlToMinify(url: String) extends AnyVal

object UrlToMinify {
  implicit val UrlToMinifyCodec: Codec[UrlToMinify] = deriveCodec[UrlToMinify]
  implicit val UrlToMinifyShow: Show[UrlToMinify]   = _.asJson.dropNullValues.noSpaces.show
}
