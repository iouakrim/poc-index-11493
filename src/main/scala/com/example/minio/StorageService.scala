package com.example.minio

import cats.effect.Async
import cats.effect.Sync
import io.minio.http.Method
import io.minio.CopyObjectArgs
import io.minio.CopySource
import io.minio.GetObjectArgs
import io.minio.GetPresignedObjectUrlArgs
import io.minio.MinioClient
import io.minio.PutObjectArgs
import io.minio.RemoveObjectArgs

import java.io.InputStream
import java.util.concurrent.TimeUnit
import scala.util.Failure
import scala.util.Success
import scala.util.Try

trait StorageService[F[_]] {
  def getObject(path: String): F[InputStream]

  def putObject(path: String, content: InputStream, contentType: String): F[Unit]

  def copyObject(source: String, destination: String): F[Unit]

  def removeObject(path: String): F[Unit]

  def getUrl(bucket: String, path: String): F[String]
}

class MinIOStorageService[F[_]: Async](client: MinioClient, bucketName: String) extends StorageService[F] {
  override def getObject(path: String): F[InputStream] =
    Async[F].blocking(
      client.getObject(
        GetObjectArgs
          .builder()
          .bucket(bucketName)
          .`object`(path)
          .build()
      )
    )

  override def putObject(path: String, content: InputStream, contentType: String): F[Unit] =
    Async[F].blocking {
      Try(
        client
          .putObject(
            PutObjectArgs
              .builder()
              .bucket(bucketName)
              .`object`(path)
              .stream(content, -1, 10485760)
              .contentType(contentType)
              .build()
          )
      ) match {
        case Failure(exception) => throw exception
        case Success(_)         => ()
      }
      ()
    }

  override def getUrl(bucket: String, path: String): F[String] =
    Async[F].blocking {
      client
        .getPresignedObjectUrl(
          GetPresignedObjectUrlArgs
            .builder()
            .method(Method.GET)
            .bucket(bucket)
            .`object`(path)
            .expiry(1, TimeUnit.HOURS)
            .build()
        )
    }

  override def copyObject(source: String, destination: String): F[Unit] =
    Sync[F].blocking {
      client
        .copyObject(
          CopyObjectArgs
            .builder()
            .bucket(bucketName)
            .`object`(destination)
            .source(
              CopySource
                .builder()
                .bucket(bucketName)
                .`object`(source)
                .build()
            )
            .build()
        )
      ()
    }

  override def removeObject(path: String): F[Unit] =
    Sync[F].blocking(
      client
        .removeObject(
          RemoveObjectArgs
            .builder()
            .bucket(bucketName)
            .`object`(path)
            .build()
        )
    )

}
