ThisBuild / organization := "com.example"
ThisBuild / scalaVersion := "2.13.5"
resolvers += Resolver.mavenLocal

lazy val root = (project in file(".")).settings(
  name := "index-11493",
  libraryDependencies ++= Seq(
    "org.typelevel"       %% "cats-effect"                 % "3.3.12",
    "org.typelevel"       %% "cats-effect-kernel"          % "3.3.12",
    "org.typelevel"       %% "cats-effect-std"             % "3.3.12",
    "org.gnieh"           %% "fs2-data-csv"                % "1.1.0",
    "org.gnieh"           %% "fs2-data-csv-generic"        % "1.1.0",
    "io.minio"             % "minio"                       % "8.3.6",
    "io.circe"            %% "circe-core"                  % "0.14.1",
    "io.circe"            %% "circe-generic-extras"        % "0.14.1",
    "io.circe"            %% "circe-generic"               % "0.14.1",
    "org.http4s"          %% "http4s-blaze-client"         % "0.23.9",
    "org.http4s"          %% "http4s-circe"                % "0.23.9",
    "org.typelevel"       %% "log4cats-core"               % "2.2.0",
    "org.typelevel"       %% "log4cats-slf4j"              % "2.2.0",
    "org.slf4j"            % "jcl-over-slf4j"              % "1.7.35",
    "org.slf4j"            % "jul-to-slf4j"                % "1.7.35",
    "org.slf4j"            % "log4j-over-slf4j"            % "1.7.35",
    "net.sf.jasperreports" % "jasperreports"               % "6.19.1",
    "net.sf.jasperreports" % "jasperreports-htmlcomponent" % "6.5.1",
    "com.itextpdf"         % "itextpdf"                    % "5.5.13.3",
    "com.itextpdf.tool"    % "xmlworker"                   % "5.5.13.3",
    "com.itextpdf"         % "html2pdf"                    % "4.0.2",
    "com.lowagie"          % "itext"                       % "2.1.7",
    // "com.itextpdf" % "itextpdf" % "5.5.13.3",

    // better monadic for compiler plugin as suggested by documentation
    compilerPlugin("com.olegpy" %% "better-monadic-for" % "0.3.1")
  )
)
